import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-magic-ball',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
